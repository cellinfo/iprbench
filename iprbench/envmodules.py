from typing import Set, List
from pathlib import Path
import subprocess
import re

EnvModule = str  # eg compilers/ifort/latest


def execute_env_module_command(module_args: str) -> List[str]:
    '''executes the given environment module command and returns stdout and stderr merged together, as the module command sometimes outputs to stdout, and sometimes to stderr, as explained here:

    [https://modules.readthedocs.io/en/latest/MIGRATING.html]
    ```
    Control output redirection

    Since version 4.0, the module function is initialized differently on sh, bash, ksh, zsh and fish shells when their session is found interactive. In such situation module redirects its output from stderr to stdout. Once initialized the redirection behavior is inherited in sub-sessions.

    The redirect_output configuration option is introduced in version 5.1, to supersede the default behavior set at initialization time.
    ```

    '''
    completed_process = subprocess.run(f'module {module_args}', executable='/bin/bash', shell=True, capture_output=True, check=True)  # for some reason, module is only found when executable is /bin/bash
    stdout = completed_process.stdout.decode('utf-8')
    stderr = completed_process.stderr.decode('utf-8')
    merged_outputs = stdout.splitlines() + stderr.splitlines()
    return merged_outputs


def get_available_modules() -> Set[EnvModule]:
    available_modules = set()
    stdouterr = execute_env_module_command('avail --terse')
    # (iprbench.venv) graffy@alambix50:/opt/ipr/cluster/work.local/graffy/bug3958/iprbench.git$ module avail --terse
    # /usr/share/modules/modulefiles:
    # compilers/ifort/15.0.2
    # compilers/ifort/17.0.1
    # ...
    # lib/mpi/intelmpi/2021.13.0
    # lib/mpi/intelmpi/latest
    # module-git
    # module-info
    # modules
    # null
    for line in stdouterr:
        if not re.search(r'\:$', line):  # ignore the directories such as '/usr/share/modules/modulefiles:'
            available_modules.add(EnvModule(line))
    return available_modules


def get_module_file_path(env_module: EnvModule) -> Path:
    # graffy@alambix-frontal:~$ module help compilers/ifort/latest
    # -------------------------------------------------------------------
    # Module Specific Help for /usr/share/modules/modulefiles/compilers/ifort/latest:

    # Provides the same functionality as the command '/opt/intel/oneapi-2024.2.1/compiler/latest/env/vars.sh intel64'
    # -------------------------------------------------------------------
    module_file_path = None
    stdouterr = execute_env_module_command(f'help {env_module}')

    for line in stdouterr:
        match = re.match(r'^Module Specific Help for (?P<module_file_path>[^:]+):', line)
        if match:
            module_file_path = Path(match['module_file_path'])
            break
    if module_file_path is None:
        raise ValueError(f'failed to find the file path of the environment module {env_module}')
    return module_file_path
