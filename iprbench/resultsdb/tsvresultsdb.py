import logging
import pandas as pd
from pathlib import Path
from ..core import IResultsDb, IResultsTable, BenchmarkParamValues, IBenchmark, IResultsDbCreator, ResultsDbParams


class TsvResultsTable(IResultsTable):
    tsv_results_dir: Path

    def __init__(self, benchmark: IBenchmark, results_db: 'TsvResultsDb', tsv_results_dir: Path):
        self.tsv_results_dir = tsv_results_dir
        super().__init__(results_db, benchmark)

    def add_benchmark(self, benchmark_record: BenchmarkParamValues):
        """adds a benchmark record to this table

            a benchmark record represents a row of values in a benchmark results table; it contains the benchmark's results, along with the configuration parameters and the BenchmarkAutoParams. For exemple { 'measurement_time': datetime.(2024, 10, 24, 16, 34, 41), 'cpu': 'intel_xeon_6348r', 'matrix_size': 1024, 'duration': 0.522}
        """
        table_file_path = self.tsv_results_dir / f'{self.benchmark.bench_id}.tsv'
        if not table_file_path.exists():
            table_file_path.parent.mkdir(parents=True, exist_ok=True)
            param_names = [param.name for param in self.get_params()]
            df = pd.DataFrame(columns=param_names)
            df.to_csv(table_file_path, sep='\t', index=False)
        logging.debug('table_file_path=%s', table_file_path)
        df = pd.read_csv(table_file_path, sep='\t')
        df.loc[len(df)] = benchmark_record
        df.to_csv(table_file_path, sep='\t', index=False)
        print(df)


class TsvResultsDb(IResultsDb):
    tsv_results_dir: Path

    def __init__(self, tsv_results_dir: Path):
        self.tsv_results_dir = tsv_results_dir
        super().__init__()

    def get_table(self, benchmark: IBenchmark) -> IResultsTable:
        table = TsvResultsTable(benchmark, self, self.tsv_results_dir)
        return table


class TsvResultsDbCreator(IResultsDbCreator):

    def __init__(self):
        super().__init__('tsv-files')

    def create_resultsdb(self, resultsdb_config: ResultsDbParams) -> IResultsDb:
        return TsvResultsDb(Path(resultsdb_config['tsv_results_dir']))
