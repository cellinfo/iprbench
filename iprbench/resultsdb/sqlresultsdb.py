from typing import List
import logging
from pathlib import Path
from cocluto.SimpaDbUtil import ISqlDatabaseBackend, SqliteDb, SqlTableField, SshAccessedMysqlDb
from ..core import IResultsDb, IResultsTable, BenchmarkParamValues, IBenchmark, IResultsDbCreator, ResultsDbParams, BenchParam


class SqlResultsTable(IResultsTable):
    sql_backend: ISqlDatabaseBackend

    def __init__(self, benchmark: IBenchmark, results_db: 'SqlResultsDb', sql_backend: ISqlDatabaseBackend):
        self.sql_backend = sql_backend
        super().__init__(results_db, benchmark)

    def add_benchmark(self, benchmark_record: BenchmarkParamValues):
        """adds a benchmark record to this table

            a benchmark record represents a row of values in a benchmark results table; it contains the benchmark's results, along with the configuration parameters and the BenchmarkAutoParams. For exemple { 'measurement_time': datetime.(2024, 10, 24, 16, 34, 41), 'cpu': 'intel_xeon_6348r', 'matrix_size': 1024, 'duration': 0.522}
        """
        table_name = self.get_sql_table_name()
        params = self.get_params()
        if not self.sql_backend.table_exists(table_name):
            logging.debug('creating table %s because it doesn\'t already exist', table_name)

            fields: List[SqlTableField] = []

            index_name = 'measure_id'
            fields.append(SqlTableField(index_name, SqlTableField.Type.FIELD_TYPE_INT, 'unique identifier of the measurement', is_autoinc_index=True))
            for param in params:
                param: BenchParam
                sql_field_type = {
                    BenchParam.Type.PARAM_TYPE_FLOAT: SqlTableField.Type.FIELD_TYPE_FLOAT,
                    BenchParam.Type.PARAM_TYPE_INT: SqlTableField.Type.FIELD_TYPE_INT,
                    BenchParam.Type.PARAM_TYPE_STRING: SqlTableField.Type.FIELD_TYPE_STRING,
                    BenchParam.Type.PARAM_TYPE_TIME: SqlTableField.Type.FIELD_TYPE_TIME,
                    BenchParam.Type.PARAM_TYPE_PACKAGE: SqlTableField.Type.FIELD_TYPE_STRING,  # packages are stored as strings in the form ifort:2021.1.2
                }[param.param_type]
                fields.append(SqlTableField(param.name, sql_field_type, param.description))
            self.sql_backend.create_table(table_name, fields)
        assert self.sql_backend.table_exists(table_name)
        # "use iprbenchs; insert into $strTableName(measure_time, computer_name, num_cpus, cpu_model, computer_id, matrix_size, mkl_version, bench_duration, num_mmul, mmul_duration, gflops) values('$strNow', '$(Util_getMyHostName)', '$(Util_getNumCpus)', '$(Util_getCpuName)', '$(Util_getHardwareSerialNumber)', '$iMatrixSize', '$strMklVersion', '$fBenchDuration', '$iNumLoops', '$fMeanMMulDuration', '$fGflops');"
        param_names = []
        param_values = []
        for param in params:
            param_names.append(param.name)
            param_values.append(f"'{str(benchmark_record[param.name])}'")
            logging.debug('param %s = \'%s\'', param.name, str(benchmark_record[param.name]))
        sql_add_row_command = f"INSERT INTO {table_name} ({', '.join(param_names)}) values({', '.join(param_values)});"
        _ = self.sql_backend.query(sql_add_row_command)

    def get_sql_table_name(self) -> str:
        return self.benchmark.bench_id


class SqlResultsDb(IResultsDb):
    sql_backend: ISqlDatabaseBackend

    def __init__(self, sql_backend: ISqlDatabaseBackend):
        self.sql_backend = sql_backend
        super().__init__()

    def get_table(self, benchmark: IBenchmark) -> IResultsTable:
        table = SqlResultsTable(benchmark, self, self.sql_backend)
        return table


class SqliteResultsDbCreator(IResultsDbCreator):

    def __init__(self):
        super().__init__('sqlite-database')

    def create_resultsdb(self, resultsdb_config: ResultsDbParams) -> IResultsDb:
        sql_backend = SqliteDb(Path(resultsdb_config['sqlite_file_path']))
        return SqlResultsDb(sql_backend)


class SqlServerResultsDbCreator(IResultsDbCreator):
    """creates a SqlResultsDb instance configured using a RemoteMysqlDb backend
    """
    def __init__(self):
        super().__init__('sqlserver-viassh-database')

    def create_resultsdb(self, resultsdb_config: ResultsDbParams) -> IResultsDb:
        db_server_fqdn = resultsdb_config['db_server_fqdn']
        db_user = resultsdb_config['db_user']
        db_name = resultsdb_config['db_name']
        ssh_user = resultsdb_config['ssh_user']
        sql_backend = SshAccessedMysqlDb(db_server_fqdn, db_user, db_name, ssh_user)
        # sql_backend = RemoteMysqlDb(db_server_fqdn, db_user, db_name)
        return SqlResultsDb(sql_backend)
