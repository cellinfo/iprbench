from typing import Dict
import subprocess
import re
from pathlib import Path
from .core import ITargetHost, PackageId, PackageVersion, HostTypeId
from .envmodules import EnvModule, get_available_modules, get_module_file_path
# import logging


DebianPackageVersion = str  # a version string, as in debian package versions, eg 4:9.3.0-1ubuntu2
DebianPackageId = str  # the identifier of a package in debian repositories (eg libopenblas0-pthread)


class DebianHost(ITargetHost):

    def get_host_type_id(self) -> HostTypeId:
        return HostTypeId('debian')

    def get_package_default_version(self, package_id: PackageId) -> PackageVersion:
        package_version = ''
        if self.is_installed_os_package(package_id):
            package_version = self.get_installed_package_version(package_id)
        return package_version

    def get_package_activation_command(self, package_id: PackageId, package_version: str) -> str:
        current_version = self.get_package_default_version(package_id)
        if current_version != package_version:
            raise ValueError(f'{package_id} version {package_version} not available: only {package_id} version {current_version} is available on this host')
        else:
            return ''  # no special instructions are required to activate the current package version

    def _get_debian_default(self, debian_generic_name: str) -> PackageVersion:
        debian_default = None
        completed_process = subprocess.run(f'update-alternatives --get-selections | grep "^{debian_generic_name} "', shell=True, check=False, capture_output=True)
        if completed_process == 0:
            raise ValueError(f'{debian_generic_name} is not a debian generic name listed by `update-alternatives --get-selections`')
        else:
            first_line = completed_process.stdout.decode('utf-8').split('\n')[0]
            debian_default = first_line.split(' ')[-1]
        return debian_default

    @staticmethod
    def debian_version_to_version(debian_version: DebianPackageVersion) -> PackageVersion:
        """
        [https://serverfault.com/questions/604541/debian-packages-version-convention]

        >The format is: [epoch:]upstream_version[-debian_revision]

        """
        # expected to return '9.3.0' for '4:9.3.0-1ubuntu2'
        match = re.match(r'^(?:(?P<epoch>[0-9]+):|)(?P<upstream_version>[0-9\.]+)(?P<debian_revision>.*)$', debian_version)
        assert match, f'unexpected format for debian_version: "{debian_version}"'
        version = PackageVersion(match['upstream_version'])
        return version

    @staticmethod
    def which(executable: str) -> Path:
        completed_process = subprocess.run(f'which {executable}', shell=True, check=True, capture_output=True)
        first_line = completed_process.stdout.decode('utf-8').splitlines()[0]
        return Path(first_line)

    @staticmethod
    def get_debian_package_providing_file(file_path: Path) -> DebianPackageId:
        completed_process = subprocess.run(f'dpkg -S {file_path}', shell=True, check=True, capture_output=True)
        first_line = completed_process.stdout.decode('utf-8').splitlines()[0]
        match = re.match(r'^(?P<debian_package_id>[a-z0-9\-]+): (?P<file_path>[^$]+)$', first_line)
        assert match, f'unexpected results for dpkg -S {file_path}'
        return DebianPackageId(match['debian_package_id'])

    @staticmethod
    def package_id_to_debian_package_id(package_id: PackageId) -> DebianPackageId:
        debian_package_id = ''
        if package_id in ['gfortran']:
            # in debian, the default gfortran compiler comes from the package `gfortran-9`, not `gfortran` (which is only a container package that depends on gfortran-9)
            # so, the most reliable way to find that the package is gfortran-9 is to identify the package that provides the command gfortran
            exec_path = DebianHost.which(package_id)  # eg /usr/bin/gfortran
            real_exec_path = exec_path.resolve()
            debian_package_id = DebianHost.get_debian_package_providing_file(real_exec_path)
        else:
            gene_to_deb: Dict[PackageId, DebianPackageId] = {
                'libopenblas-pthread': 'libopenblas0-pthread'
            }
            if package_id in gene_to_deb.keys():
                debian_package_id = gene_to_deb[package_id]
            else:
                # we assume the ids are the same
                debian_package_id = DebianPackageId(package_id)
        return debian_package_id

    PackageAttr = str  # the name of a package attribute, as seen in dpkg -s <debian_package_id>
    PackageValue = str  # the name of a package value, as seen in dpkg -s <debian_package_id>

    @staticmethod
    def get_debian_package_status(debian_package_id: DebianPackageId) -> Dict[PackageAttr, PackageValue]:
        package_status = {}
        # 20241120-11:26:59 graffy@graffy-ws2:~/work/starbench/iprbench.git$ dpkg -s libopenblas0-pthread
        # Package: libopenblas0-pthread
        # Status: install ok installed
        # Priority: optional
        # Section: libs
        # Installed-Size: 93686
        # Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
        # Architecture: amd64
        # Multi-Arch: same
        # Source: openblas
        # Version: 0.3.8+ds-1ubuntu0.20.04.1
        # Provides: libblas.so.3, liblapack.so.3
        # Depends: libc6 (>= 2.29), libgfortran5 (>= 8)
        # Breaks: libatlas3-base (<< 3.10.3-4~), libblas3 (<< 3.7.1-2~), liblapack3 (<< 3.7.1-2~), libopenblas-dev (<< 0.2.20+ds-3~)
        # Description: Optimized BLAS (linear algebra) library (shared lib, pthread)
        #  OpenBLAS is an optimized BLAS library based on GotoBLAS2 1.13 BSD version.
        #  .
        #  Unlike Atlas, OpenBLAS provides a multiple architecture library.
        #  .
        #  All kernel will be included in the library and dynamically switched to the
        #  best architecture at run time (only on amd64, arm64, i386 and ppc64el).
        #  .
        #  For more information on how to rebuild locally OpenBLAS, see the section:
        #  "Building Optimized OpenBLAS Packages on your ARCH" in README.Debian
        #  .
        #  Configuration: USE_THREAD=1 USE_OPENMP=0 INTERFACE64=0
        # Homepage: https://github.com/xianyi/OpenBLAS
        # Original-Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
        completed_process = subprocess.run(f'dpkg -s {debian_package_id}', check=True, shell=True, capture_output=True)
        stdout = completed_process.stdout.decode('utf-8').splitlines()
        for line in stdout:
            match = re.match(r'^(?P<attr_name>[A-Za-z\-]+): (?P<attr_value>.*)$', line)
            if match:
                package_status[match['attr_name']] = match['attr_value']
        return package_status

    def is_installed_os_package(self, package_id: PackageId) -> bool:
        debian_package_id = DebianHost.package_id_to_debian_package_id(package_id)
        package_status = DebianHost.get_debian_package_status(debian_package_id)
        return package_status['Status'] == 'install ok installed'

    def get_installed_package_version(self, package_id: PackageId) -> PackageVersion:
        debian_package_id = DebianHost.package_id_to_debian_package_id(package_id)
        package_status = DebianHost.get_debian_package_status(debian_package_id)
        assert package_status['Status'] == 'install ok installed'
        debian_package_version = package_status['Version']
        return DebianHost.debian_version_to_version(debian_package_version)

    def get_default_alternative(self, package_type: str) -> str:
        """gets the installed variant for the given package type

        eg returns 'openblas' for 'libblas'
        """
        # https://wiki.debian.org/DebianScience/LinearAlgebraLibraries
        debian_generic_name = {
            'libblas': 'libblas.so-x86_64-linux-gnu'
        }[package_type]
        debian_default = self._get_debian_default(debian_generic_name)

        package_id = {
            '/usr/lib/x86_64-linux-gnu/openblas-pthread/libblas.so': 'libopenblas-pthread'
        }[debian_default]
        return package_id


class IprClusterNode(DebianHost):

    def get_host_type_id(self) -> HostTypeId:
        return HostTypeId('fr.univ-rennes.ipr.cluster-node')

    def get_latest_version_for_env_module(self, package_env_module: str) -> PackageVersion:
        # package_env_module: eg compilers/ifort

        # graffy@alambix-frontal:~$ ls -l /usr/share/modules/modulefiles/compilers/ifort/latest
        # lrwxrwxrwx 1 root root 9 18 nov.  02:11 /usr/share/modules/modulefiles/compilers/ifort/latest -> 2021.13.1

        available_modules = get_available_modules()
        shortcut_env_module = EnvModule(f'{package_env_module}/latest')
        if shortcut_env_module not in available_modules:
            raise ValueError(f'failed to find {shortcut_env_module} amongst available environment modules ({available_modules})')
        # logging.debug('available_modules = %s', available_modules)
        shortcut_file_path = get_module_file_path(shortcut_env_module)
        # logging.debug('shortcut_file_path = %s', shortcut_file_path)
        assert shortcut_file_path.is_symlink(), f'unexpected case: {shortcut_file_path} is expected to be a symbolic link to an actual version of {package_env_module}'
        real_file_path = shortcut_file_path.resolve()  # eg /usr/share/modules/modulefiles/compilers/ifort/2021.13.1
        # logging.debug('real_file_path = %s', real_file_path)
        version = real_file_path.name
        match = re.match(r'^[0-9]+\.[0-9]+\.[0-9]$', version)
        assert match, f'unexpected format for version: {version} for module file path {real_file_path}'
        return PackageVersion(version)

    def get_package_default_version(self, package_id: PackageId) -> PackageVersion:
        env_modules_packages = self._get_env_modules_packages()
        if package_id in env_modules_packages.keys():
            return self.get_latest_version_for_env_module(env_modules_packages[package_id])
        else:
            return super().get_package_default_version(package_id)

    def get_package_activation_command(self, package_id: PackageId, package_version: str) -> str:
        env_modules_packages = self._get_env_modules_packages()
        if package_id in env_modules_packages.keys():
            env_module = EnvModule(f'{env_modules_packages[package_id]}/{package_version}')
            assert env_module in get_available_modules(), f'environment module "{env_module}" is not one of the available modules on this system'
            return f'module load {env_module}'
        else:
            return super().get_package_activation_command(package_id, package_version)

    def _get_env_modules_packages(self) -> Dict[PackageId, str]:
        """ gets list packages available as environment modules"""
        return {
            PackageId('ifort'): 'compilers/ifort',
            PackageId('intelmkl'): 'lib/mkl',
        }

    def _get_package_env_module_root(self, package_id: PackageId) -> str:
        return self._get_env_modules_packages()[package_id]
