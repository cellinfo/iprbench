from typing import List
import logging
import argparse
from pathlib import Path
import json
from .core import BenchmarkId, IBenchmark, ResultsDbFactory, BenchParam
from .targethosts import DebianHost, IprClusterNode
from .benchmarks.hibridon import HibridonBenchmark
from .benchmarks.mamul1 import MaMul1
from .resultsdb.tsvresultsdb import TsvResultsDbCreator
from .resultsdb.sqlresultsdb import SqliteResultsDbCreator, SqlServerResultsDbCreator
from .util import Singleton
from .autoparams import MeasurementTime, HostFqdn, User, NumCpus, CpuModel, IprBenchVersion, HostId, OutputUrl


class BenchmarkFactory(metaclass=Singleton):

    def __init__(self):
        pass

    def create_benchmark(self, bench_id: BenchmarkId, common_params: List[BenchParam]) -> IBenchmark:
        benchmark = {
            'hibridon': HibridonBenchmark(common_params),
            'mamul1': MaMul1(common_params)
        }[bench_id]
        return benchmark


def main():
    """

    """
    logging.basicConfig(level=logging.DEBUG)

    example_text = '''example:

    %(prog)s --benchmark-id 'mamul1' --config '{"compiler_id": "gfortran", "matrix_size": 1024, "num_loops":10, "num_cores":2}' --results-dir /tmp/mamul1_out

    '''

    arg_parser = argparse.ArgumentParser(description='executes a benchmark in a cluster job environment', epilog=example_text, formatter_class=argparse.RawDescriptionHelpFormatter)
    arg_parser.add_argument('--benchmark-id', type=BenchmarkId, required=True, help='the benchmark id of the benchmark to perform (eg mamul1)')
    arg_parser.add_argument('--results-dir', type=Path, required=True, help='the root directory of the tree where the results of the benchmarks are stored (eg $GLOBAL_WORK_DIR/graffy/benchmarks/hibridon)')
    arg_parser.add_argument('--config', type=str, required=True, help='the benchmark configuration in json format, eg {"compiler_id": "gfortran", "matrix_size": 1024}')
    arg_parser.add_argument('--resultsdb-params', type=str, required=True, help='the resultsdb configuration in json format, eg {"type": "tsv-files", "tsv_results_dir": "/tmp/toto"}')
    arg_parser.add_argument('--target-system-type-id', type=str, required=True, help='id of the operating system type to use. This is used to get the list installed packages, how to activate them, etc, eg "debian", "fr.univ-rennes.ipr.cluster-node".')

    args = arg_parser.parse_args()

    results_dir = args.results_dir

    ResultsDbFactory().register_resultsdb_creator(TsvResultsDbCreator())
    ResultsDbFactory().register_resultsdb_creator(SqliteResultsDbCreator())
    ResultsDbFactory().register_resultsdb_creator(SqlServerResultsDbCreator())
    resultsdb_params = json.loads(args.resultsdb_params)
    results_db = ResultsDbFactory().create_resultsdb(resultsdb_params['type'], resultsdb_params)

    results_db.add_auto_param(MeasurementTime())
    results_db.add_auto_param(IprBenchVersion())
    results_db.add_auto_param(HostId())
    results_db.add_auto_param(HostFqdn())
    results_db.add_auto_param(User())
    results_db.add_auto_param(NumCpus())
    results_db.add_auto_param(CpuModel())
    results_db.add_auto_param(OutputUrl(args.results_dir))
    results_db.add_common_param(BenchParam('launcher', BenchParam.Type.PARAM_TYPE_STRING, description='what triggered the benchmark (eg "alambix.job.12345", or "manual")'))

    target_host = {
        'debian': DebianHost(),
        'fr.univ-rennes.ipr.cluster-node': IprClusterNode(),
    }[args.target_system_type_id]

    benchmark_id = BenchmarkId(args.benchmark_id)
    benchmark = BenchmarkFactory().create_benchmark(benchmark_id, results_db.common_params)
    benchmark_config = benchmark.load_config(args.config, target_host)

    results_table = results_db.get_table(benchmark)

    measurements = benchmark.execute(benchmark_config, results_dir, target_host)
    results_table.add_results(benchmark_config, measurements)

    # out_params.append(BenchParam('host_id', BenchParam.Type.PARAM_TYPE_STRING, 'the id of the host running the benchmark'))
    # benchmark.get_measurements(results_dir)
