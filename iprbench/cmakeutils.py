from .core import PackageId


def get_bla_vendor(package_id: PackageId) -> str:
    """returns the bla vendor used by cmake's FindBLAS module
    see https://cmake.org/cmake/help/latest/module/FindBLAS.html
    """
    bla_vendor = {
        'libopenblas-pthread': 'OpenBLAS',
        'intelmkl': 'Intel10_64lp',    # use 64 bits intel mkl with multithreading
    }[package_id]
    return bla_vendor
