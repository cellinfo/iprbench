from typing import List
from pathlib import Path
import subprocess
import shutil
from ..core import IBenchmark, BenchParam, BenchmarkConfig, BenchmarkMeasurements, ITargetHost
from ..cmakeutils import get_bla_vendor
from ..util import get_proxy_env_vars
from starbench.core import StarbenchResults


class HibridonBenchmark(IBenchmark):
    """Hibridon benchmark using starbench
    """

    def __init__(self, common_params: List[BenchParam]):
        bench_params = []
        bench_params.append(BenchParam('num_cores', BenchParam.Type.PARAM_TYPE_INT, 'the total number of cores to use by this benchmark'))
        bench_params.append(BenchParam('hibridon_version', BenchParam.Type.PARAM_TYPE_STRING, 'the version of hibridon, in the form of a commit id'))
        bench_params.append(BenchParam('fortran_compiler', BenchParam.Type.PARAM_TYPE_PACKAGE, 'the compiler used in the benchmark'))
        bench_params.append(BenchParam('blas_library', BenchParam.Type.PARAM_TYPE_PACKAGE, 'the blas compatible linear algebra library used in the benchmark'))
        bench_params.append(BenchParam('test_id', BenchParam.Type.PARAM_TYPE_STRING, 'the name of the test to run (eg arch4_quick (about 2s on a core i5 8th generation) or nh3h2_qma_long (about 10min on a core i5 8th generation))'))
        bench_params.append(BenchParam('cmake_path', BenchParam.Type.PARAM_TYPE_STRING, 'the location of the cmake executable to use (eg "/opt/cmake/cmake-3.23.0/bin/cmake", or simply "cmake" for the one in the path)'))

        out_params = []
        out_params.append(BenchParam('duration_avg', BenchParam.Type.PARAM_TYPE_FLOAT, 'the average duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_med', BenchParam.Type.PARAM_TYPE_FLOAT, 'the median duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_stddev', BenchParam.Type.PARAM_TYPE_FLOAT, 'the standard deviation of duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_min', BenchParam.Type.PARAM_TYPE_FLOAT, 'the min duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_max', BenchParam.Type.PARAM_TYPE_FLOAT, 'the max duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('num_threads_per_run', BenchParam.Type.PARAM_TYPE_INT, 'the number of cores to use by each concurrent run of the app (must be a divider of num_cores)'))

        super().__init__(bench_id='hibridon', bench_params=bench_params, out_params=out_params, common_params=common_params)

    def get_ram_requirements(self, config: BenchmarkConfig) -> int:
        GIBIBYTE_TO_BYTE = 1024 * 1024 * 1024
        ram_per_core = 0  # in bytes
        benchmark_test = config['test_id']
        if benchmark_test == 'arch4_quick':
            ram_per_core = int(1.0 * GIBIBYTE_TO_BYTE)
        elif benchmark_test == 'nh3h2_qma_long':
            ram_per_core = int(2.8 * GIBIBYTE_TO_BYTE)  # this was enough on physix48, but maybe we can reduce more
        else:
            assert f'unhandled benchmark_test : {benchmark_test}'
        return ram_per_core * config['num_cores']

    def execute(self, config: BenchmarkConfig, benchmark_output_dir: Path, target_host: ITargetHost) -> BenchmarkMeasurements:

        git_repos_url = 'https://github.com/hibridon/hibridon'
        hibridon_version = config['hibridon_version']
        test_id = config['test_id']  # eg arch4_quick or nh3h2_qma_long
        fortran_compiler = config['fortran_compiler']
        blas_library = config['blas_library']
        cmake_path = config['cmake_path']
        num_cores = config['num_cores']

        if benchmark_output_dir.exists():
            shutil.rmtree(benchmark_output_dir)
        benchmark_output_dir.mkdir(parents=True)

        src_dir = benchmark_output_dir / 'hibridon.git'
        output_dir = benchmark_output_dir / 'output'

        source_tree_provider = f'{{"type": "git-cloner", "repos-url": "{git_repos_url}", "src-dir": "{src_dir}", "code-version": "{hibridon_version}"}}'
        benchmark_command = f'ctest --output-on-failure -L ^{test_id}$'

        cmake_options = [
            '-DCMAKE_BUILD_TYPE=Release',  # build in release mode for highest performance
            '-DBUILD_TESTING=ON'  # enable hibridon tests
        ]

        package_activation_commands = []
        for param in [fortran_compiler, blas_library]:
            env_command = target_host.get_package_activation_command(param.package_id, param.package_version)
            if env_command != '':
                package_activation_commands.append(env_command)
        env_vars_bash_commands = ' && '.join(package_activation_commands)

        bla_vendor = get_bla_vendor(blas_library.package_id)
        cmake_options.append(f'-DCMAKE_Fortran_COMPILER={fortran_compiler.package_id}')
        cmake_options.append(f'-DBLA_VENDOR={bla_vendor}')

        output_measurements_file_path = output_dir / "measurements.tsv"

        shell_command = ''
        if len(env_vars_bash_commands) > 0:
            shell_command += f'{env_vars_bash_commands} && '
        shell_command += f'{get_proxy_env_vars()} starbench --source-tree-provider \'{source_tree_provider}\' --num-cores {num_cores} --output-dir={output_dir} --cmake-path={cmake_path} {" ".join([f"--cmake-option={option}" for option in cmake_options])} --benchmark-command=\'{benchmark_command}\' --output-measurements={output_measurements_file_path}'
        completed_process = subprocess.run(shell_command, shell=True, check=False, executable='/bin/bash')  # for some reason, module is only found when executable is /bin/bash
        if completed_process.returncode != 0:
            assert False, f'the shell command "{shell_command}" failed. See details in {output_dir}'
        measurements: BenchmarkMeasurements = {}
        starbench_results = StarbenchResults(output_measurements_file_path)

        num_threads_per_run = 1  # at the moment starbench always allocates 1 core per process, but in the future, starbench will support multiple cores per process (useful to measure the how the app scales with increasing parallelism)
        measurements['num_threads_per_run'] = num_threads_per_run
        measurements['duration_avg'] = starbench_results.get_average_duration()
        measurements['duration_med'] = starbench_results.get_median_duration()
        measurements['duration_stddev'] = starbench_results.get_duration_stddev()
        (measurements['duration_min'], measurements['duration_max']) = starbench_results.get_duration_range()
        return measurements

    # def get_measurements(self, benchmark_output_dir: Path) -> BenchmarkMeasurements:
    #     raise NotImplementedError()
