from typing import List
from pathlib import Path
import subprocess
import logging
from ..core import IBenchmark, BenchParam, BenchmarkConfig, BenchmarkMeasurements, ITargetHost
from ..cmakeutils import get_bla_vendor
from iprbench.util import extract_resource_dir
from starbench.core import StarbenchResults


class MaMul1(IBenchmark):
    """Matrix multiplication benchmark
    """
    def __init__(self, common_params: List[BenchParam]):
        bench_params = []
        bench_params.append(BenchParam('fortran_compiler', BenchParam.Type.PARAM_TYPE_PACKAGE, 'the compiler used in the benchmark'))
        bench_params.append(BenchParam('blas_library', BenchParam.Type.PARAM_TYPE_PACKAGE, 'the blas compatible linear algebra library used in the benchmark'))
        bench_params.append(BenchParam('num_cores', BenchParam.Type.PARAM_TYPE_INT, 'the number of cores to use by this benchmark'))
        bench_params.append(BenchParam('matrix_size', BenchParam.Type.PARAM_TYPE_INT, 'the size n of all the the n * n matrices'))
        bench_params.append(BenchParam('num_loops', BenchParam.Type.PARAM_TYPE_INT, 'the number of identical multiplications performed in sequence'))
        # bench_params.append(BenchParam('source_dir', BenchParam.Type.PARAM_TYPE_STRING, 'the path to the directory containing mamul1 test source files'))

        out_params = []
        out_params.append(BenchParam('duration_avg', BenchParam.Type.PARAM_TYPE_FLOAT, 'the average duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_med', BenchParam.Type.PARAM_TYPE_FLOAT, 'the median duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_stddev', BenchParam.Type.PARAM_TYPE_FLOAT, 'the standard deviation of duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_min', BenchParam.Type.PARAM_TYPE_FLOAT, 'the min duration of one matrix multiplication, in seconds'))
        out_params.append(BenchParam('duration_max', BenchParam.Type.PARAM_TYPE_FLOAT, 'the max duration of one matrix multiplication, in seconds'))

        super().__init__(bench_id='mamul1', bench_params=bench_params, out_params=out_params, common_params=common_params)

    def get_ram_requirements(self, config: BenchmarkConfig) -> int:
        GIBIBYTE_TO_BYTE = 1024 * 1024 * 1024
        SIZE_OF_DOUBLE = 8  # in bytes
        matrix_size = config['matrix_size']
        matrix_ram_size = matrix_size * matrix_size * SIZE_OF_DOUBLE
        num_matrices = 3
        ram_requirements = int(1 * GIBIBYTE_TO_BYTE) + num_matrices * matrix_ram_size
        return ram_requirements

    def execute(self, config: BenchmarkConfig, benchmark_output_dir: Path, target_host: ITargetHost) -> BenchmarkMeasurements:
        fortran_compiler = config['fortran_compiler']
        blas_library = config['blas_library']
        num_cores = config['num_cores']
        matrix_size = config['matrix_size']
        num_loops = config['num_loops']

        # extract the mamul1 source code tree from iprbench's resources
        mamul1_source_code_root_path = benchmark_output_dir / 'mamul1'
        extract_resource_dir('iprbench.resources', 'mamul1', dest_path=mamul1_source_code_root_path)

        output_dir = benchmark_output_dir / 'output'

        source_tree_provider = f'{{"type": "existing-dir", "dir-path": "{mamul1_source_code_root_path}"}}'
        benchmark_command = ['./mamul1', f'{matrix_size}', f'{num_loops}']

        cmake_options = [
            '-DCMAKE_BUILD_TYPE=Release',  # build in release mode for highest performance
        ]

        package_activation_commands = []
        for param in [fortran_compiler, blas_library]:
            env_command = target_host.get_package_activation_command(param.package_id, param.package_version)
            if env_command != '':
                package_activation_commands.append(env_command)
        env_vars_bash_commands = ' && '.join(package_activation_commands)

        bla_vendor = get_bla_vendor(blas_library.package_id)
        cmake_options.append(f'-DCMAKE_Fortran_COMPILER={fortran_compiler.package_id}')
        cmake_options.append(f'-DBLA_VENDOR={bla_vendor}')

        output_measurements_file_path = output_dir / "measurements.tsv"

        shell_command = ''
        if len(env_vars_bash_commands) > 0:
            shell_command += f'{env_vars_bash_commands} && '
        shell_command += f'starbench --source-tree-provider \'{source_tree_provider}\' --num-cores {num_cores} --output-dir={output_dir} --cmake-path=/usr/bin/cmake {" ".join([f"--cmake-option={option}" for option in cmake_options])} --benchmark-command=\'{" ".join(benchmark_command)}\' --output-measurements={output_measurements_file_path}'
        logging.debug('shell_command = "%s"', shell_command)
        completed_process = subprocess.run(shell_command, shell=True, check=False, executable='/bin/bash')  # for some reason, module is only found when executable is /bin/bash
        if completed_process.returncode != 0:
            assert False, f'the shell command "{shell_command}" failed. See details in {output_dir}'
        measurements: BenchmarkMeasurements = {}
        starbench_results = StarbenchResults(output_measurements_file_path)
        measurements['duration_avg'] = starbench_results.get_average_duration()
        measurements['duration_med'] = starbench_results.get_median_duration()
        measurements['duration_stddev'] = starbench_results.get_duration_stddev()
        (measurements['duration_min'], measurements['duration_max']) = starbench_results.get_duration_range()
        return measurements

    # def get_measurements(self, benchmark_output_dir: Path) -> BenchmarkMeasurements:
    #     raise NotImplementedError()
