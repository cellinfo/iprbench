from datetime import datetime
import socket
import subprocess
import re
import os
import pwd
from pathlib import Path
from .core import IAutoParam, BenchParam, BenchParamType
from .version import __version__ as iprbench_version


class MeasurementTime(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('measurement_time', BenchParam.Type.PARAM_TYPE_TIME, 'the time (and date) at which this measurment has been made')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        return datetime.now()


class IprBenchVersion(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('ipr_bench_version', BenchParam.Type.PARAM_TYPE_STRING, 'the version of iprbench used for this measurement')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        return iprbench_version


class HostId(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('host_id', BenchParam.Type.PARAM_TYPE_STRING, 'the serial number of the host running the benchmark')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        serial_number = '<unknown>'
        completed_process = subprocess.run(r"dmidecode | grep -A4 '^System Information' | grep 'Serial Number' | sed 's/^\s*Serial Number: //'; exit $PIPESTATUS[0]", check=False, shell=True, capture_output=True)
        if completed_process.returncode == 0:
            serial_number = completed_process.stdout.decode(encoding='utf-8').split('\n')[0]
        return serial_number


class HostFqdn(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('host_fqdn', BenchParam.Type.PARAM_TYPE_STRING, 'the fully qualified domain name of the host running the benchmark')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        return socket.getfqdn()


class User(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('user', BenchParam.Type.PARAM_TYPE_STRING, 'the user that triggered the benchmark (eg "root")')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        return pwd.getpwuid(os.getuid())[0]


class NumCpus(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('num_cpus', BenchParam.Type.PARAM_TYPE_INT, 'the number of cpus on the benchmarked system')
        super().__init__(bench_param)

    def get_value(self) -> BenchParamType:
        num_cpus = 0
        completed_process = subprocess.run(r"cat /proc/cpuinfo | awk 'BEGIN { maxid=0;  } /^physical id/ {if ($4 > maxid) { maxid = $4 };} END {print maxid+1;}'", check=False, shell=True, capture_output=True)
        if completed_process.returncode == 0:
            num_cpus = int(completed_process.stdout.decode(encoding='utf-8').split('\n')[0])
        return num_cpus


class CpuModel(IAutoParam):

    def __init__(self):
        bench_param = BenchParam('cpu_model', BenchParam.Type.PARAM_TYPE_STRING, 'The exact model of the cpu running the benchmark eg "Intel(R) Core(TM) i5-8350U CPU @ 1.70GHz"')
        super().__init__(bench_param)

    @staticmethod
    def model_name_to_cpu_model_id(cpu_model_name: str) -> str:
        """
        cpu_model_name: the name of a cpu as seen in /proc/cpuinfo (eg 'Intel(R) Core(TM) i5-8350U CPU @ 1.70GHz')
        return: a compact identifier of the cpu model without spaces (eg 'intel_core_i5_8350u' (the frequency is implicit, as this model only operates at 1.7 Ghz))
        """
        match = re.match(r'Intel\(R\) Core\(TM\) i(?P<major_id>[357])-(?P<minor_id>[0-9]+[U]) CPU @ [0-9]+\.[0-9]+GHz', cpu_model_name)
        if match:
            return f'intel_core_i{match["major_id"]}_{match["minor_id"].lower()}'

        # eg "Intel(R) Xeon(R) Gold 6248R CPU @ 3.00GHz"
        match = re.match(r'Intel\(R\) Xeon\(R\) Gold +(?P<xeon_id>[^ ]+) +CPU +@ [0-9]+\.[0-9]+GHz', cpu_model_name)
        if match:
            return f'intel_xeon_gold_{match["xeon_id"].lower()}'

        # eg "Intel(R) Xeon(R) CPU           X5650  @ 2.67GHz"
        # eg 'Intel(R) Xeon(R) CPU E5-2660 0 @ 2.20GHz': 'intel_xeon_e5-2660',
        # eg 'Intel(R) Xeon(R) CPU E5-2660 v2 @ 2.20GHz': 'intel_xeon_e5-2660v2'
        # eg'Intel(R) Xeon(R) CPU E5-2660 v4 @ 2.00GHz': 'intel_xeon_e5-2660v4'
        match = re.match(r'Intel\(R\) Xeon\(R\) CPU +(?P<xeon_id>[^ ]+) +(?P<version>(?:[^@ ]+ +|))@ [0-9]+\.[0-9]+GHz', cpu_model_name)
        if match:
            cpu_id = f'intel_xeon_{match["xeon_id"].lower()}'
            version = match['version']
            m2 = re.match(r'^v(?P<version_number>[0-9]+) ', version)
            if m2:
                cpu_id += f'v{m2["version_number"]}'
            return cpu_id

        # eg "AMD EPYC 7282 16-Core Processor"
        match = re.match(r'AMD EPYC (?P<epyc_model_id>[0-9a-z]+) (?P<num_cores>[0-9]+)-Core Processor', cpu_model_name)
        if match:
            return f'amd_epyc_{match["epyc_model_id"].lower()}'

        assert False, f'unhandled cpu model name: "{cpu_model_name}"'

        return None

    def get_value(self) -> BenchParamType:
        # completed_process = subprocess.run('grep "^model name +:" /proc/cpuinfo | head -1 | sed "s/model name *: //"', shell=True, check=True, capture_output=True)
        completed_process = subprocess.run('grep -E "^model name\\s+:" /proc/cpuinfo', shell=True, check=False, capture_output=True)
        cpu_model_lines = completed_process.stdout.decode().split('\n')[:-1]
        # print(cpu_model_lines)
        total_num_cores = len(cpu_model_lines)
        print(f'total number of cores (including virtual cores) on this host : {total_num_cores}')
        cpu_model_line = cpu_model_lines[0]  # eg "model name      : Intel(R) Core(TM) i5-8350U CPU @ 1.70GHz"
        match = re.match(r'model name\s+: (?P<cpu_model>.*)$', cpu_model_line)
        cpu_model = match['cpu_model']
        # print(cpu_model)
        return CpuModel.model_name_to_cpu_model_id(cpu_model)


class OutputUrl(IAutoParam):

    def __init__(self, output_dir: Path):
        bench_param = BenchParam('output_url', BenchParam.Type.PARAM_TYPE_STRING, 'the location of the output files of this benchmarks')
        super().__init__(bench_param)
        self.output_url = f'file://{socket.getfqdn()}{output_dir.absolute()}'

    def get_value(self) -> BenchParamType:
        return self.output_url
