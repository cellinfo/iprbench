import unittest
import logging
import subprocess
import json
from pathlib import Path
from iprbench.core import BenchmarkConfig, BenchmarkId, HostTypeId
from shutil import rmtree


def test_clusterbench_submit_with_benchmark(benchmark_id: BenchmarkId, benchmark_config: BenchmarkConfig, results_root_path: Path):
    logging.info('testing clusterbench_submit on benchmark %s', benchmark_id)
    results_dir = Path(f'{results_root_path}/{benchmark_id}')
    if results_dir.exists():
        rmtree(results_dir)
    results_dir.mkdir(parents=True)
    # output results into a tsv file
    resultsdb_params = {
        'type': 'tsv-files',
        'tsv_results_dir': f'{results_dir / "results"}'
    }
    target_system_type_id = HostTypeId('debian')
    command = f'clusterbench-submit --cluster-id \'dummy\' --arch-regexp "intel_core.*" --benchmark-id \'{benchmark_id}\' --config \'{json.dumps(benchmark_config)}\' --results-dir {results_dir} --resultsdb-params \'{json.dumps(resultsdb_params)}\' --target-system-type-id "{target_system_type_id}"'
    subprocess.run(command, shell=True, check=True, executable='/bin/bash')


class ClusterBenchTestCase(unittest.TestCase):

    results_root_dir: Path

    def setUp(self) -> None:  # pylint: disable=useless-parent-delegation
        self.results_root_dir = Path('/tmp/iprbenchs/test_results/clusterbench_submit')
        if self.results_root_dir.exists():
            rmtree(self.results_root_dir)
        self.results_root_dir.mkdir(parents=True)
        return super().setUp()

    def test_clusterbench_submit1(self):
        benchmark_id = 'mamul1'
        benchmark_config = {
            'fortran_compiler': 'gfortran:<default>',
            'blas_library': '<default-libblas>:<default>',
            'matrix_size': 1024,
            'num_loops': 10,
            'launcher': 'iprbench.unittest',
        }
        test_clusterbench_submit_with_benchmark(benchmark_id, benchmark_config, self.results_root_dir)


if __name__ == '__main__':
    unittest.main()
