"""tests all IResultsDb implementations
"""
import unittest
import logging
import subprocess
import json
from pathlib import Path
from shutil import rmtree
from os import getenv
from iprbench.core import ResultsDbParams, HostTypeId
from cocluto.SimpaDbUtil import SshAccessedMysqlDb


def test_resultsdb(resultsdb_params: ResultsDbParams, results_root_path: Path):
    results_db_type = resultsdb_params['type']
    logging.info('testing resultsdb %s', results_db_type)
    logging.info('resultsdb_params : %s', json.dumps(resultsdb_params))
    results_dir = results_root_path / results_db_type
    if results_dir.exists():
        rmtree(results_dir)
    results_dir.mkdir(parents=True)
    benchmark_id = 'mamul1'
    benchmark_config = {
        'fortran_compiler': 'gfortran:<default>',
        'blas_library': '<default-libblas>:<default>',
        'matrix_size': 1024,
        'num_loops': 10,
        'num_cores': 2,
        'launcher': 'iprbench.unittest',
    }
    target_system_type_id = HostTypeId('debian')
    command = f'iprbench-run --benchmark-id \'{benchmark_id}\' --config \'{json.dumps(benchmark_config)}\' --results-dir {results_dir} --resultsdb-params \'{json.dumps(resultsdb_params)}\' --target-system-type-id "{target_system_type_id}"'
    subprocess.run(command, shell=True, check=True, executable='/bin/bash')


class ResultsDbTestCase(unittest.TestCase):

    results_root_dir: Path

    def setUp(self) -> None:  # pylint: disable=useless-parent-delegation
        self.results_root_dir = Path(f'{getenv("TMPDIR", default="/tmp")}/iprbenchs/test_results/resultsdb')
        if self.results_root_dir.exists():
            rmtree(self.results_root_dir)
        self.results_root_dir.mkdir(parents=True)
        return super().setUp()

    def test_tsvfiles(self):
        resultsdb_params = {
            'type': 'tsv-files',
            'tsv_results_dir': str(self.results_root_dir / 'tsv-files')
        }
        test_resultsdb(resultsdb_params, self.results_root_dir)

    def test_sqlitedatabase(self):
        resultsdb_params = {
            'type': 'sqlite-database',
            'sqlite_file_path': str(self.results_root_dir / 'sqlite-database/results.sqlite')
        }
        test_resultsdb(resultsdb_params, self.results_root_dir)

    def test_sqlserver(self):
        db_server_fqdn = 'iprbenchsdb.ipr.univ-rennes1.fr'
        db_user = 'test_iprbenchw'
        db_name = 'test_iprbenchs'
        ssh_user = 'test_iprbenchw'
        sql_backend = SshAccessedMysqlDb(db_server_fqdn, db_user, db_name, ssh_user)
        if sql_backend.table_exists('mamul1'):
            sql_backend.delete_table('mamul1')
        resultsdb_params = {
            'type': 'sqlserver-viassh-database',
            'db_server_fqdn': db_server_fqdn,
            'db_user': db_user,
            'db_name': db_name,
            'ssh_user': ssh_user
        }
        test_resultsdb(resultsdb_params, self.results_root_dir)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
