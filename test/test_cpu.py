"""test all benchmarks
"""
import unittest
import logging
from pathlib import Path
from iprbench.autoparams import CpuModel


class CpuTestCase(unittest.TestCase):

    results_root_dir: Path

    def setUp(self) -> None:  # pylint: disable=useless-parent-delegation
        return super().setUp()

    def test_cpu(self):
        # amd_epyc_7282
        # amd_epyc_7282
        # intel_xeon_x5550
        # intel_xeon_x5650
        # intel_xeon_e5-2660
        # intel_xeon_e5-2660v2
        # intel_xeon_e5-2660v4
        # intel_xeon_gold_5220
        # intel_xeon_gold_6140
        # intel_xeon_gold_6154
        # intel_xeon_gold_6226r
        # intel_xeon_gold_6248r
        ipr_cpus_id = {
            r'Intel(R) Core(TM) i5-8350U CPU @ 1.70GHz': 'intel_core_i5_8350u',
            r'Intel(R) Xeon(R) CPU           X5650  @ 2.67GHz': 'intel_xeon_x5650',
            r'Intel(R) Xeon(R) CPU E5-2660 0 @ 2.20GHz': 'intel_xeon_e5-2660',
            r'Intel(R) Xeon(R) CPU E5-2660 v2 @ 2.20GHz': 'intel_xeon_e5-2660v2',
            r'Intel(R) Xeon(R) CPU E5-2660 v4 @ 2.00GHz': 'intel_xeon_e5-2660v4',
            r'Intel(R) Xeon(R) Gold 6248R CPU @ 3.00GHz': 'intel_xeon_gold_6248r',
            r'AMD EPYC 7282 16-Core Processor': 'amd_epyc_7282',
            r'AMD EPYC 7452 32-Core Processor': 'amd_epyc_7452',
        }
        for cpuinfo_id, short_id in ipr_cpus_id.items():
            self.assertEqual(CpuModel.model_name_to_cpu_model_id(cpuinfo_id), short_id)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
