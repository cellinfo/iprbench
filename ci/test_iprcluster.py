"""tests specific for IPR cluster
"""
import unittest
import logging
import subprocess
import json
from pathlib import Path
from shutil import rmtree
from os import getenv
from iprbench.core import HostTypeId


class IprClusterTestCase(unittest.TestCase):

    results_root_dir: Path

    def setUp(self) -> None:  # pylint: disable=useless-parent-delegation
        self.results_root_dir = Path(f'{getenv("TMPDIR", default="/tmp")}/iprbenchs/test_results/ipr-cluster')
        if self.results_root_dir.exists():
            rmtree(self.results_root_dir)
        self.results_root_dir.mkdir(parents=True)
        return super().setUp()

    def test_cluster_node_host_type(self):
        '''a simple test that uses IprClusterNode class to build a simple code with intel fortran and intel mkl'''
        resultsdb_params = {
            'type': 'tsv-files',
            'tsv_results_dir': str(self.results_root_dir / 'tsv-files')
        }
        logging.info('resultsdb_params : %s', json.dumps(resultsdb_params))
        results_dir = self.results_root_dir / 'ipr-cluster-node'
        if results_dir.exists():
            rmtree(results_dir)
        results_dir.mkdir(parents=True)
        benchmark_id = 'mamul1'
        benchmark_config = {
            'fortran_compiler': 'ifort:<default>',
            'blas_library': 'intelmkl:<default>',
            'matrix_size': 1024,
            'num_loops': 10,
            'num_cores': 2,
            'launcher': 'iprbench.unittest.iprcluster',
        }
        target_system_type_id = HostTypeId('fr.univ-rennes.ipr.cluster-node')
        command = f'iprbench-run --benchmark-id \'{benchmark_id}\' --config \'{json.dumps(benchmark_config)}\' --results-dir {results_dir} --resultsdb-params \'{json.dumps(resultsdb_params)}\' --target-system-type-id "{target_system_type_id}"'
        subprocess.run(command, shell=True, check=True, executable='/bin/bash')


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
